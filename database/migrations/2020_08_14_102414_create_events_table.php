<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('project_name')->comment('Имя проекта');
            $table->float('price')->comment('Цена');
            $table->string('type_work')->comment('Тип работ');
            $table->integer('company_id')->comment('Компания');
            $table->integer('employee_id')->comment('Ответсвенный сотрудник компании');
            $table->date('date')->comment('Дата');
            $table->integer('shift')->comment('Рабочая смена');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
