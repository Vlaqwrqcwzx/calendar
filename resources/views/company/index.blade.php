@extends('layouts.app')

@section('content')
    @include('navigation')

    <div class="container p-2">
        <div class="row p-2">
            <a href="{{ route('companies.create') }}" class="btn btn-success">Добавить компанию</a>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-5">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>Название</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($companies as $k => $company)
                        <tr>
                            <td>{{ $k + 1 }}</td>
                            <td>{{ $company->name }}</td>
                            <td>
                                <a href="{{ route('companies.edit', $company) }}"
                                   class="btn btn-block btn-primary btn-sm">edit</a>
                                <form method="post" action="{{ route('companies.destroy', $company) }}"
                                      accept-charset="UTF-8" class="btn-block">
                                    @csrf
                                    <input type="hidden" name="_method" value="delete"/>
                                    <span class="glyphicon glyphicon-align-left" aria-hidden="true"></span>
                                    <input class="btn btn-danger btn-block btn-sm" type="submit" value="delete"/>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
