@extends('layouts.app')

@section('content')
    @include('navigation')
    <div class="container p-2">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form action="{{ route('companies.store') }}" method="POST">
                    @csrf
                    <fieldset class="border p-2">
                        <legend class="w-auto btn-sm text-primary">Форма добавления компании</legend>
                        <div class="form-group">
                            <label for="name">Название</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ @old('name') }}"
                                   placeholder="Название">
                        </div>
                        <div class="form-group p-2">
                            <button type="submit" class="btn btn-outline-success">Сохранить</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection
