<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="list-group">
                <a href="{{ route('home') }}"
                   class="list-group-item {{ (request()->is('home*')) ? 'active' : 'list-group-item-action' }}">Главная</a>
                <a href="{{ route('employees.index') }}"
                   class="list-group-item {{ (request()->is('employees*')) ? 'active' : 'list-group-item-action' }}">Сотрудники</a>
                <a href="{{ route('companies.index') }}"
                   class="list-group-item {{ (request()->is('companies*')) ? 'active' : 'list-group-item-action' }}">Компании</a>
            </div>
        </div>
    </div>
</div>
