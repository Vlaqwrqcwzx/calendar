@extends('layouts.app')

@section('content')
    @include('navigation')

    @include('event.create')

    @include('event.index')
@endsection
