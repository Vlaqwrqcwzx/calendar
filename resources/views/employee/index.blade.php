@extends('layouts.app')

@section('content')
    @include('navigation')

    <div class="container p-2">
        <div class="row p-2">
            <a href="{{ route('employees.create') }}" class="btn btn-success">Добавить сотрудника</a>
        </div>
        <div class="row justify-content-center">
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Фамилия</th>
                    <th>Имя</th>
                    <th>Отчество</th>
                    <th>Компания</th>
                    <th>Email</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($employees as $k => $employee)
                    <tr>
                        <td>{{ $k + 1 }}</td>
                        <td>{{ $employee->surname }}</td>
                        <td>{{ $employee->name }}</td>
                        <td>{{ $employee->patronymic }}</td>
                        <td>{{ $employee->company->name }}</td>
                        <td>{{ $employee->email }}</td>
                        <td>
                            <a href="{{ route('employees.edit', $employee) }}"
                               class="btn btn-block btn-primary btn-sm">edit</a>
                            <form method="post" action="{{ route('employees.destroy', $employee) }}"
                                  accept-charset="UTF-8" class="btn-block">
                                @csrf
                                <input type="hidden" name="_method" value="delete"/>
                                <span class="glyphicon glyphicon-align-left" aria-hidden="true"></span>
                                <input class="btn btn-danger btn-block btn-sm" type="submit" value="delete"/>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
