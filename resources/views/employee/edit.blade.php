@extends('layouts.app')

@section('content')
    @include('navigation')
    <div class="container p-2">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form action="{{ route('employees.update', $employee) }}" method="POST">
                    {{ method_field('PATCH') }}
                    @csrf
                    <fieldset class="border p-2">
                        <legend class="w-auto btn-sm text-primary">Форма редактирования сотрудника</legend>
                        <div class="form-group">
                            <label for="surname">Фамилия</label>
                            <input type="text" class="form-control" id="surname" name="surname"
                                   value="{{ $employee->surname }}" placeholder="Фамилия">
                        </div>

                        <div class="form-group">
                            <label for="name">Имя</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ $employee->name }}"
                                   placeholder="Имя">
                        </div>

                        <div class="form-group">
                            <label for="patronymic">Отчество</label>
                            <input type="text" class="form-control" id="patronymic" name="patronymic"
                                   value="{{ $employee->patronymic }}" placeholder="Отчество">
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email"
                                   value="{{ $employee->email }}" placeholder="Email">
                        </div>

                        <div class="form-group">
                            <label for="company_id">Компания</label>
                            <select id="company_id" class="form-control" name="company_id">
                                @foreach($companies as $company)
                                    <option {{ ($employee->company_id == $company->id) ? 'selected' : '' }}
                                        value="{{ $company->id }}">{{ $company->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group p-2">
                            <button type="submit" class="btn btn-outline-success">Обновить</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection
