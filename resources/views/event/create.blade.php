<div class="container p-2">
    <div class="row">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#eventModel">
            Добавить событие
        </button>

        <div class="modal fade" id="eventModel" tabindex="-1" role="dialog" aria-labelledby="eventModel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModal3Label">Новое событие</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="eventForm">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="project_name">Название проекта</label>
                                <input type="text" class="form-control" id="project_name" name="project_name"
                                       placeholder="Название проекта" required>
                            </div>

                            <div class="form-group">
                                <label for="name">Стоимость работ</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">$</span>
                                    </div>
                                    <input type="number" value="1000" min="0" step="0.01" data-number-to-fixed="2"
                                           data-number-stepfactor="100" class="form-control currency" id="price"
                                           name="price"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="type_work">Тип работ</label>
                                <input type="text" class="form-control" id="type_work" name="type_work"
                                       placeholder="Название проекта" required>
                            </div>

                            <div class="form-group">
                                <label for="company_id">Компания</label>
                                <select id="company_id" class="form-control" name="company_id" required>
                                    <option value="" selected disabled hidden>Choose here</option>
                                    @foreach($companies as $company)
                                        <option value="{{ $company->id }}">{{ $company->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group employee_input">
                                <label for="employee_id">Ответственный сотрудник</label>
                                <select id="employee_id" class="form-control" name="employee_id" required>
                                    <option value="" selected disabled hidden>Choose here</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="date">Дата</label>
                                <input type="date" class="form-control" id="date" name="date"
                                       placeholder="Дата" required>
                            </div>

                            <div class="form-group">
                                <label for="shift">Смена</label>
                                <select id="shift" class="form-control" name="shift" required>
                                    <option value="" selected disabled hidden>Choose here</option>
                                    <option value="1">Первая смена</option>
                                    <option value="2">Вторая смена</option>
                                    <option value="3">Третья смена</option>
                                </select>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>