<div class="container p-2">
    <div class="row justify-content-center">
        @foreach($companiesWithEvents as $companyWithEvent)
            <table class="table">
                <thead>
                <tr class="table-info text-center">
                    <th colspan="7">{{ $companyWithEvent->name }}</th>
                </tr>
                <tr class="thead-light">
                    <th>Название проекта</th>
                    <th>Стоимость работ</th>
                    <th>Тип работ</th>
                    <th>Дата</th>
                    <th>Смена</th>
                    <th>Сотрудник</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($companyWithEvent->events as $event)
                    <tr>
                        <td>{{ $event->project_name }}</td>
                        <td>{{ $event->price }} $</td>
                        <td>{{ $event->type_work }}</td>
                        <td>{{ $event->date->day }} {{ $event->date->formatLocalized('%B') }}
                            , {{ $event->date->year }}</td>
                        <td>{{ $event->shift }}</td>
                        <td>{{ $event->employee->surname . ' ' . $event->employee->name . ' ' . $event->employee->patronymic }}</td>
                        <td>
                            <a href="{{ route('events.edit', $event) }}"
                               class="btn btn-block btn-primary btn-sm">edit</a>
                            <form method="post" action="{{ route('events.destroy', $event) }}"
                                  accept-charset="UTF-8" class="btn-block">
                                @csrf
                                <input type="hidden" name="_method" value="delete"/>
                                <span class="glyphicon glyphicon-align-left" aria-hidden="true"></span>
                                <input class="btn btn-danger btn-block btn-sm" type="submit" value="delete"/>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endforeach
    </div>
</div>

