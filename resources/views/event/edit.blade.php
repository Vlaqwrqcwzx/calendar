@extends('layouts.app')

@section('content')
    @include('navigation')
    <div class="container p-2">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form action="{{ route('events.update', $event) }}" method="POST">
                    {{ method_field('PATCH') }}
                    @csrf
                    <fieldset class="border p-2">
                        <legend class="w-auto btn-sm text-primary">Форма редактирования события</legend>
                        <div class="form-group">
                            <label for="project_name">Название проекта</label>
                            <input type="text" class="form-control" id="project_name" name="project_name"
                                   value="{{ $event->project_name }}" required>
                        </div>

                        <div class="form-group">
                            <label for="name">Стоимость работ</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input type="number" value="{{ $event->price }}" min="0" step="0.01"
                                       data-number-to-fixed="2"
                                       data-number-stepfactor="100" class="form-control currency" id="price"
                                       name="price"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="type_work">Тип работ</label>
                            <input type="text" value="{{ $event->type_work }}" class="form-control" id="type_work"
                                   name="type_work" placeholder="Название проекта" required>
                        </div>

                        <div class="form-group">
                            <label for="company_id">Компания</label>
                            <select id="company_id" class="form-control" name="company_id" required>
                                <option value="" selected disabled hidden>Choose here</option>
                                @foreach($companies as $company)
                                    <option value="{{ $company->id }}" {{ ($company->id === $event->company_id) ? 'selected' : ''}}>{{ $company->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group employee_input">
                            <label for="employee_id">Ответственный сотрудник</label>
                            <select id="employee_id" class="form-control" name="employee_id" required>
                                <option value="{{ $employee->id }}"
                                        selected>{{ $employee->surname }} {{ $employee->name }}  {{ $employee->patronymic }}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="date">Дата</label>
                            <input type="date" value="{{ $event->date->format('Y-m-d') }}" class="form-control"
                                   id="date" name="date"
                                   placeholder="Дата" required>
                        </div>

                        <div class="form-group">
                            <label for="shift">Смена</label>
                            <select id="shift" class="form-control" name="shift" required>
                                <option value="" selected disabled hidden>Choose here</option>
                                <option value="1" {{ ($event->shift === 1) ? 'selected' : '' }}>Первая смена</option>
                                <option value="2" {{ ($event->shift === 2) ? 'selected' : '' }}>Вторая смена</option>
                                <option value="3" {{ ($event->shift === 3) ? 'selected' : '' }}>Третья смена</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-primary">Обновить</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('.employee_input').show();
        });
    </script>
@stop