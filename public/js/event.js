$(document).ready(function () {

    var employee_input = $('.employee_input');
    employee_input.hide();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#company_id').on('change', function () {
        $('#employee_id').find('option').remove()
        $('#employee_id').append(new Option("Choose here", "", true))
        $.ajax({
            type: 'POST',
            url: GlobalRoutes.getEmployees,
            data: {
                company_id: this.value
            },
            success: function (data) {
                employee_input.show()
                data.forEach(function (val) {
                    $('#employee_id').append(new Option(val["surname"] + ' ' + val["name"] + ' ' + val["patronymic"], val["id"]))
                })
            }
        });
    });

    $('#eventForm').submit(function (e) {
        e.preventDefault()

        $.ajax({
            type: 'POST',
            url: GlobalRoutes.events_store,
            data: $('#eventForm').serialize(),
            success: function (data) {
                console.log(data)
                $("#eventModel").modal('hide');
                jQuery('#eventForm')[0].reset();
            }
        });
    })


});