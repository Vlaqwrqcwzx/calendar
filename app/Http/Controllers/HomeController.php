<?php

namespace App\Http\Controllers;

use App\Models\Company;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $companies = Company::all();
        $companiesWithEvents = Company::with('events', 'events.employee')->get();
        return view('home', compact('companies', 'companiesWithEvents'));
    }
}
