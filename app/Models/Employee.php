<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['surname', 'name', 'patronymic', 'company_id', 'email'];

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }
}
