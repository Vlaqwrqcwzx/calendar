<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['name'];

    protected $table = 'companies';

    public function employees()
    {
        return $this->hasMany('App\Models\Employee');
    }

    public function responsible_employees()
    {
        return $this->hasMany('App\Models\Employee')->where('responsible', 1);
    }

    public function events()
    {
        return $this->hasMany(Event::class)
            ->orderBy('date', 'asc')
            ->orderBy('shift', 'asc');
    }
}
